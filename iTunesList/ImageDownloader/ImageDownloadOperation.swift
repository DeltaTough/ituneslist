import Foundation

class ImageImportOperation: AsyncOperation, FileProtocol {
    var url: URL
    
    init(url: URL) {
        self.url = url
        super.init()
    }
    
    override func main() {
        guard !isCancelled else { return }
        // local Path
        guard let localPath = ImageImportOperation.localFilePath(url: url) else {
            finish()
            return
        }
        // already downloaded
        if FileManager.default.fileExists(atPath: localPath.absoluteString) {
            finish()
            return
        }
        
        guard let imageData = try? Data(contentsOf: url) else {finish(); return }
        saveImage(localPath: localPath, image: imageData)
        finish()
    }
    
    
    static func getLocalImageDataFor(url: URL) -> Data? {
        guard let localFilePath = ImageImportOperation.localFilePath(url: url) else {
            return nil
        }
        return try? Data(contentsOf: localFilePath)
    }
    
    static func localFilePath(url: URL) -> URL? {
        let docs = try? FileManager.default.url(for:.cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return docs?.appendingPathComponent("images").appendingPathComponent(url.path)
    }
    
    internal func saveImage(localPath: URL, image: Data) {
        do {
            try FileManager.default.createDirectory(at: localPath.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
            try image.write(to: localPath)
        } catch {
            print("ERROR SAVING IMAGE: \(error.localizedDescription)")
        }
    }
}

protocol FileProtocol {
    static func getLocalImageDataFor(url: URL) -> Data?
    static func localFilePath(url: URL) -> URL?
    func saveImage(localPath: URL, image: Data)
}

