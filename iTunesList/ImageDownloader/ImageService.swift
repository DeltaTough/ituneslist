import Foundation
import UIKit

class ImageService: NSObject {
    let queue = OperationQueue()
    
    static var shared = ImageService()
    private override init() {
        super.init()
        self.queue.qualityOfService = .background
        self.queue.maxConcurrentOperationCount = 4
        self.queue.addObserver(self, forKeyPath: "operations", options: .new, context: nil)
    }
    
    deinit {
        self.cancelAll()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let object = object as? OperationQueue, object == queue, let keyPath = keyPath, keyPath == "operations" {
            print("ImageService Queue operations: \(queue.operationCount)")
            return
        }
        print("observeValue UNEXPECTED!!! \(String(describing: keyPath)) | \(String(describing: object)) | \(String(describing: change))")
        super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
    
    func cancelAll() {
        self.queue.cancelAllOperations()
    }
    
    @discardableResult func downloadImage(url: URL, completion: @escaping () -> Void) -> ImageImportOperation? {
        let op = ImageImportOperation(url: url)
        op.completionBlock = completion
        self.queue.addOperation(op)
        return op
    }
}


