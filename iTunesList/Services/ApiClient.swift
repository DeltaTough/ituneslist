import RxCocoa
import RxSwift
import UIKit

enum HttpMethod: String {
    case get = "GET"
}

final class ApiClient {
    var urlSession = URLSession(configuration: .default)

    static func createRequest(urlString: String, method: HttpMethod = .get, data: Data? = nil) -> URLRequest? {
        guard let url = URL(string: urlString) else {
            return nil
        }
        var req = URLRequest(url: url)
        req.httpMethod = method.rawValue
        if method != .get, data != nil {
            req.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        }
        req.setValue("application/json", forHTTPHeaderField: "Accept")
        req.httpBody = data

        return req
    }

    func send<T>(request: URLRequest, withDateFormatter dateFormatter: DateFormatter? = nil) -> Observable<Result<T, ApiError>> where T: Decodable {
        return urlSession.rx.response(request: request)
            .map { response, data in
                if T.self == Bool.self, let isSuccess = (200 ..< 300 ~= response.statusCode) as? T {
                    return .success(isSuccess)
                }

                if 200 ..< 300 ~= response.statusCode {
                    return ApiClient.deserialise(data: data, withDateFormatter: dateFormatter)
                }
                return .failure(.generic)
            }
            .catch { error in ApiClient.catchError(error: error) }
    }

    private static func deserialise<T>(data: Data, withDateFormatter dateFormatter: DateFormatter? = nil) -> Result<T, ApiError> where T: Decodable {
        let decoder = JSONDecoder()
        if let dateFormatter = dateFormatter {
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
        } else {
            // MEMO: this does not like fractions of a second
            decoder.dateDecodingStrategy = .iso8601
        }
        do {
            return try .success(decoder.decode(T.self, from: data))
        } catch {
            var message = "Decode of \(String(describing: T.self)) has failed."
            if let stringData = String(data: data, encoding: .utf8) {
                message.append(" data: \(stringData)")
            }
            return .failure(.deserialisationFailed)
        }
    }

    private static func catchError<T>(error: Error) -> Observable<Result<T, ApiError>> {
        let error = error as NSError
        if error.domain == NSURLErrorDomain, error.code == NSURLErrorNotConnectedToInternet {
            return Observable.just(.failure(.noConnection))
        }
        return Observable.just(.failure(.generic))
    }
}



