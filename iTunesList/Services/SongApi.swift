import RxSwift
import Foundation

struct SongApi: Decodable {
    let results: [SongResult]
}

struct SongResult: Decodable {
    let trackId: Int
    let trackName: String
    let artistName: String
    let trackViewUrl: String
    let artworkUrl100: String
    let trackPrice: Double
    let releaseDate: Date
    let trackTimeMillis: Int
    let currency: String
}

protocol SongsApiClient {
    func getSongs() -> Observable<Result<[SongResult], ApiError>>
}

extension ApiClient: SongsApiClient {
    func getSongs() -> Observable<Result<[SongResult], ApiError>> {
        guard let request = ApiClient.createRequest(urlString: EndPoints.rockSongs.rawValue) else {
            return Observable.just(.failure(.generic))
        }

        return send(request: request, withDateFormatter: iso8601withFractionsDateFormatter)
            .map { (result: Result<SongApi, ApiError>) in
                result.map {
                    return $0.results
                }
            }
    }
}
