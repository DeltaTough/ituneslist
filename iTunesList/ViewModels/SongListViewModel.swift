import RxCocoa

struct SongListViewModel {
    let isBusy = BehaviorRelay<Bool>(value: false)

    private let songsApiClient: SongsApiClient

    init(songsApiClient: SongsApiClient = ApiClient()) {
        self.songsApiClient = songsApiClient
    }

    func getSongs() -> Driver<[SongResult]> {
        isBusy.accept(true)
        return songsApiClient.getSongs()
            .map { try $0.get() }
            .asDriver(onErrorJustReturn: [])
            .do(onCompleted: {
                self.isBusy.accept(false)
            }, onSubscribe: {
                self.isBusy.accept(true)
            })
    }
}
