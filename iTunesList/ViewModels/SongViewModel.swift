import Foundation

struct SongViewModel {
    var releaseDate: String
    var trackTime: String
    var price: String?
    var songName: String
    var detailsViewURL: String
    var imageURL: String
    var artist: String
}

extension SongViewModel {
    init(song: SongResult) {
        self.releaseDate = getDate(date: song.releaseDate)
        self.trackTime = getTrackTime(time: song.trackTimeMillis)
        if let decimal = Decimal(string: String(song.trackPrice)) {
            self.price = decimal.formatWith(currencyCode: song.currency)
        }
        self.songName = song.trackName
        self.detailsViewURL = song.trackViewUrl
        self.imageURL = song.artworkUrl100
        self.artist = song.artistName
    }
}

private func getDate(date: Date) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateStyle = .short
    dateFormat.locale = .current
    return dateFormat.string(from: date)
}

private func getTrackTime(time: Int) -> String {
    let minutes = (time / (1000 * 60)) % 60
    let seconds = (time / 1000) % 60
    let secAddExtraZeroIfNeeded = String(format: "%02d", seconds)
    return "\(minutes):\(secAddExtraZeroIfNeeded)"
}
