import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        Theme.setupNavigationBar()
        goToSongs()
        window?.makeKeyAndVisible()
        return true
    }

    func goToSongs() {
        let list = AppStoryboard.songList.instance.instantiateInitialViewController()
        window?.rootViewController = list
    }
}

