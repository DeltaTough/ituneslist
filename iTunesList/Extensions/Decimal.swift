import UIKit

extension Decimal {
    func formatWith(currencyCode: String, maximumFractionDigits: Int = 2) -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = maximumFractionDigits
        formatter.numberStyle = .currency
        formatter.currencyCode = currencyCode

        return formatter.string(from: self as NSDecimalNumber) ?? ""
    }
}
