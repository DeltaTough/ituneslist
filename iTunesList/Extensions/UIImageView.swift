import UIKit

extension UIImageView {
    func getShowImageForURL(imageURL: String?) {
        if let imageURL = imageURL, let url = URL(string: imageURL) {
            if let imageData = ImageImportOperation.getLocalImageDataFor(url: url) {
                DispatchQueue.main.async { [weak self] in
                    self?.image = UIImage(data: imageData)
                }
            } else {
                ImageService.shared.downloadImage(url: url) { [weak self] in
                    DispatchQueue.main.async {  [weak self] in
                        self?.refreshImage(imageURL: url)
                    }
                }
            }
        }
    }
    
    fileprivate func refreshImage(imageURL: URL) {
        guard let imageData = ImageImportOperation.getLocalImageDataFor(url: imageURL) else {
            return
        }
        self.image = UIImage(data: imageData)
    }
}
