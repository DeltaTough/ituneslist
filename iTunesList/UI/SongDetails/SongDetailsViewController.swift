import RxCocoa
import RxSwift
import UIKit

class SongDetailsViewController: UIViewController {
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!

    var viewModel: SongViewModel?
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = viewModel else { fail() }
        artistImageView.getShowImageForURL(imageURL: viewModel.imageURL)
        nameLabel.text = viewModel.songName
        artistLabel.text = viewModel.artist
        priceLabel.text = viewModel.price
        durationLabel.text = viewModel.trackTime
        dateLabel.text = viewModel.releaseDate

        moreButton.layer.cornerRadius = 8
        moreButton.layer.borderWidth = 1
        moreButton.layer.borderColor = UIColor.darkGray.cgColor

        moreButton.rx.tap.asObservable().bind { _ in
            if let url = URL(string: viewModel.detailsViewURL) {
                UIApplication.shared.open(url)
            }
        }.disposed(by: disposeBag)
    }
}
