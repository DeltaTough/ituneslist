import UIKit

class SongCell: UITableViewCell {
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var artistImageView: UIImageView!

    var viewModel: SongViewModel? {
        didSet {
            guard let viewModel = viewModel else { fail() }

            trackLabel?.text = viewModel.songName
            artistLabel?.text = viewModel.artist
            priceLabel.text = viewModel.price
            artistImageView.getShowImageForURL(imageURL: viewModel.imageURL)
        }
    }
}

