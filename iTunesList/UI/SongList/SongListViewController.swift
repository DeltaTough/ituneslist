import RxCocoa
import RxSwift
import UIKit

class SongListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private let results = BehaviorRelay<[SongResult]>(value: [])
    private var viewModel = SongListViewModel()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Rock Tracks"

        viewModel.getSongs()
            .drive(onNext: {
                self.results.accept($0.sorted(by: { $0.releaseDate > $1.releaseDate })) })
            .disposed(by: self.disposeBag)

        viewModel.isBusy.asDriver().drive(activityIndicator.rx.isAnimating).disposed(by: disposeBag)
        viewModel.isBusy.asDriver().drive(tableView.rx.isHidden).disposed(by: disposeBag)

        results.bind(to: tableView.rx.items(cellIdentifier: "SongCell",
                                            cellType: SongCell.self)) { _, result, cell in
            cell.viewModel = SongViewModel(song: result)
        }.disposed(by: disposeBag)

        tableView.rx.modelSelected(SongResult.self)
            .subscribe(onNext: { [weak self] result in
                DispatchQueue.main.async {
                    if let vc = AppStoryboard.songDetails.viewController(for: SongDetailsViewController.self) {
                        vc.viewModel = SongViewModel(song: result)
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }).disposed(by: disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: index, animated: true)
        }
    }
}
