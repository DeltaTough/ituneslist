import UIKit

enum AppStoryboard: String {
    case songList = "SongList"
    case songDetails = "SongDetails"

    var instance: UIStoryboard {
        UIStoryboard(name: rawValue, bundle: Bundle.main)
    }

    func viewController<T>(for type: T.Type) -> T? where T: UIViewController {
        instance.instantiateViewController(withIdentifier: String(describing: type.self)) as? T
    }
}
