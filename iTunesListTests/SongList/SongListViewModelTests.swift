@testable import iTunesList
import RxBlocking
import RxCocoa
import RxSwift
import XCTest

class SongListViewModelTests: XCTestCase {
    var mockResultApiClient: MockResultApiClient!
    var viewModel: SongListViewModel!
    var disposeBag: DisposeBag!

    override func setUp() {
        mockResultApiClient = MockResultApiClient()
        viewModel = SongListViewModel(songsApiClient: mockResultApiClient)
        disposeBag = DisposeBag()
    }

    override func tearDown() {
        disposeBag = DisposeBag()
    }

    func testFetchSongsApi() throws {
        var isBusyCalls = [Bool]()
        viewModel.isBusy.do(onNext: {
            isBusyCalls.append($0)
        }).subscribe().disposed(by: disposeBag)

        let result = try viewModel.getSongs().toBlocking().first()
        XCTAssertEqual(mockResultApiClient.getSongsCallCount, 1)
        XCTAssertTrue(result!.count > 0)
        XCTAssertEqual(isBusyCalls.count, 4)
        XCTAssertFalse(isBusyCalls[0])
        XCTAssertTrue(isBusyCalls[1])
        XCTAssertTrue(isBusyCalls[2])
        XCTAssertFalse(isBusyCalls[3])
    }
}
