@testable import iTunesList
import RxSwift

class MockResultApiClient: SongsApiClient {
    var result: Result<[SongResult], ApiError> = Result.success([createSong()])
    var error: Error?
    var getSongsCallCount = 0

    func getSongs() -> Observable<Result<[SongResult], ApiError>> {
        getSongsCallCount += 1
        if let error = error {
            return Observable.error(error)
        } else {
            return Observable.just(result)
        }
    }
}
