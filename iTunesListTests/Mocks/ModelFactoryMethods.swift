@testable import iTunesList
import UIKit

func createSong(
    trackId: Int = 0,
    trackName: String = "",
    artistName: String = "",
    trackViewUrl: String = "",
    artworkUrl100: String = "",
    trackPrice: Double = 0.0,
    releaseDate: Date = Date(),
    trackTimeMillis: Int = 0,
    currency: String = "") -> SongResult {
        SongResult(trackId: trackId,
                   trackName: trackName,
                   artistName: artistName,
                   trackViewUrl: trackViewUrl,
                   artworkUrl100: artworkUrl100,
                   trackPrice: trackPrice,
                   releaseDate: releaseDate,
                   trackTimeMillis: trackTimeMillis,
                   currency: currency)
    }
