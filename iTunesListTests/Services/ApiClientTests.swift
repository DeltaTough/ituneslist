@testable import iTunesList
import RxBlocking
import RxSwift
import XCTest

class ApiClientTests: XCTestCase {
    var apiClient: ApiClient!

    let mockURLSessionConfig: URLSessionConfiguration = {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]
        return config
    }()

    struct SomeIntResponse: Decodable {
        let someInt: Int
    }

    struct SomeComplexDataWithDate: Decodable {
        let createdDate: Date
        let someMessage: String
    }

    override func setUp() {
        MockURLProtocol.requestHandler = nil
        apiClient = ApiClient()
        apiClient.urlSession = URLSession(configuration: mockURLSessionConfig)
    }

    override func tearDown() {
        MockURLProtocol.requestHandler = nil
    }

    func testDeserialiseCustomDateFormatter() throws {
        let dateString = iso8601withFractionsDateFormatter.string(from: Date())
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            let data = try JSONSerialization.data(withJSONObject: [["createdDate": dateString, "someMessage": "Hello World!"]])
            return (response, data)
        }

        let request = ApiClient.createRequest(urlString:  "dummy")
        let result: Result<[SomeComplexDataWithDate], ApiError> = try apiClient.send(request: request!, withDateFormatter: iso8601withFractionsDateFormatter).toBlocking().first()!

        let resultValue = try result.get()
        XCTAssertEqual(resultValue.count, 1)
        XCTAssertEqual(resultValue.first?.createdDate, iso8601withFractionsDateFormatter.date(from: dateString))
        XCTAssertEqual(resultValue.first?.someMessage, "Hello World!")
    }

    func testSendSimpleBoolResponseFail() throws {
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 300, httpVersion: nil, headerFields: nil)!
            return (response, Data())
        }

        let request = ApiClient.createRequest(urlString: "dummy")
        let result: Result<Bool, ApiError> = try apiClient.send(request: request!).toBlocking().first()!

        XCTAssertFalse(try result.get())
    }

    func testSendCatchNoConnectionError() throws {
        MockURLProtocol.requestHandler = { _ in
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet)
        }

        let request = ApiClient.createRequest(urlString: "dummy")
        let result: Result<Bool, ApiError> = try apiClient.send(request: request!).toBlocking().first()!

        switch result {
        case .success:
            XCTFail("should have failed")
        case let .failure(error):
            XCTAssertEqual(error, .noConnection)
        }
    }

    func testDeserialiseFailOnInvalidJSON() throws {
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, Data())
        }

        let request = ApiClient.createRequest(urlString: "dummy")
        let result: Result<SomeIntResponse, ApiError> = try apiClient.send(request: request!).toBlocking().first()!
        switch result {
        case .success:
            XCTFail("should have failed")
        case let .failure(error):
            XCTAssertEqual(error, .deserialisationFailed)
        }
    }
}


